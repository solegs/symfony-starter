<?php

declare(strict_types = 1);

namespace App\Tests\Unit\Service;

use App\Service\UserService;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\String\ByteString;

final class UserServiceTest extends TestCase
{
    private UserService $service;
    private Generator $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->service = new UserService();
        $this->faker = Factory::create();
    }

    public function tearDown(): void
    {
        if (file_exists($_ENV['BLOOM_CACHE'])) {
            unlink($_ENV['BLOOM_CACHE']);
        }
    }

    /**
     * @dataProvider dataProvider
     */
    public function testIsNewUserMethod(?string $input): void
    {
        $input = $input ?? $this->faker->ipv4();

        $firstResult = $this->service->isNewUser($input);
        $this->assertSame($firstResult, true);

        $secondResult = $this->service->isNewUser($input);
        $this->assertSame($secondResult, false);
    }

    /**
     * @return iterable
     */
    public function dataProvider(): iterable
    {
        yield [ByteString::fromRandom(32)->toUnicodeString()->toString()];
        yield [null];
    }
}
