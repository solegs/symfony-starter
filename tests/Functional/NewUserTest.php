<?php

declare(strict_types = 1);

namespace App\Tests\Functional;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\String\ByteString;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class NewUserTest extends ApiTestCase
{
    public function tearDown(): void
    {
        if (file_exists($_ENV['BLOOM_CACHE'])) {
            unlink($_ENV['BLOOM_CACHE']);
        }
    }

    /**
     * @dataProvider dataProvider
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCheckIsNewUser(?string $token)
    {
        $client = static::createClient();

        $data = !is_null($token) ? ['token' => $token] : [];

        $client->request('POST', '/is-new-user', [
            'json' => $data,
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'isNewUser' => true,
        ]);

        $client->request('POST', '/is-new-user', [
            'json' => $data
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'isNewUser' => false,
        ]);
    }

    /**
     * @return iterable
     */
    public function dataProvider(): iterable
    {
        yield [ByteString::fromRandom(32)->toUnicodeString()->toString()];
        yield [null];
    }
}
