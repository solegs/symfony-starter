<?php

declare(strict_types = 1);

namespace App\Service;

class BloomFilter
{
    private int $filterSize;
    private array $filter;

    public function __construct(int $filterSize = 10000)
    {
        $this->filterSize = $filterSize;
        $this->filter = array_fill(0, $this->filterSize, false);
    }

    public function addElement(string $element): void
    {
        $hashes = $this->getHashes($element);
        foreach ($hashes as $hash) {
            $this->filter[$hash] = true;
        }
    }

    public function containsElement(string $element): bool
    {
        $hashes = $this->getHashes($element);
        foreach ($hashes as $hash) {
            if (!$this->filter[$hash]) {
                return false;
            }
        }

        return true;
    }

    private function getHashes(string $element): array
    {
        return [
            crc32($element) % $this->filterSize
        ];
    }
}
