<?php

declare(strict_types = 1);

namespace App\Service;

class UserService
{
    private BloomFilter $bloomFilter;

    public function __construct()
    {
        $this->bloomFilter = file_exists($_ENV['BLOOM_CACHE']) ? unserialize(file_get_contents($_ENV['BLOOM_CACHE'])) : new BloomFilter();
    }

    public function isNewUser(string $input): bool
    {
        if ($this->bloomFilter->containsElement($input)) {
            return false;
        }

        $this->bloomFilter->addElement($input);

        file_put_contents($_ENV['BLOOM_CACHE'], serialize($this->bloomFilter));

        return true;
    }
}
