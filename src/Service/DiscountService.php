<?php

declare(strict_types = 1);

namespace App\Service;

class DiscountService
{
    public function applyDiscount(array $items, int $discount): array
    {
        $itemsTotal = $this->getItemsTotal($items);

        if ($itemsTotal <= $discount) {
            return array_map(function (array $item): array {
                $item['price_with_discount'] = 0;

                return $item;
            }, $items);
        } else {
            return $this->getDiscountedItems($items, $discount, $itemsTotal);
        }
    }

    private function getItemsTotal(array $items): int
    {
        return array_reduce($items, function (int $carry, array $item): int
        {
            $carry += $item['price'];

            return $carry;
        }, 0);
    }

    private function getDiscountedItems(array $items, int $discount, int $itemsTotal): array
    {
        $leftDiscount = $discount;
        $percentDiscount = $discount / ($itemsTotal / 100);
        $discountedItems = [];

        foreach ($items as $item) {
            if ($leftDiscount > 0) {
                $itemDiscount = round(($item['price'] / 100) * $percentDiscount);
            } else {
                $itemDiscount = 0;
            }

            if ($leftDiscount < $itemDiscount) {
                $itemDiscount = $leftDiscount;
            }

            $priceWithDiscount = $item['price'] >= $itemDiscount ? $item['price'] - $itemDiscount : 0;
            $item['price_with_discount'] = (int) $priceWithDiscount;
            $leftDiscount -= $itemDiscount;
            $discountedItems[] = $item;
        }

        if ($leftDiscount > 0) {
            $discountedItems[0]['price_with_discount'] -= $leftDiscount;
        }

        return $discountedItems;
    }
}