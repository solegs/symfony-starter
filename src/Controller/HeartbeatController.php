<?php

declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeartbeatController extends AbstractController
{
    #[Route('/heartbeat', name: 'heartbeat', methods: ['GET'])]
    public function index(Request $request): Response
    {
        return $this->json([], Response::HTTP_OK);
    }
}
