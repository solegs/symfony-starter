<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Voucher;
use App\Repository\VoucherRepository;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateVoucherController extends AbstractController
{
    public function __construct(
        private readonly VoucherRepository $voucherRepository
    ) {
    }

    #[Route('/generate', name: 'create_voucher', methods: ['POST'])]
    public function index(Request $request): Response
    {
        $json = $request->getContent();

        try {
            $data = json_decode($json, true, flags: JSON_THROW_ON_ERROR);

            if(empty($data['discount']) || !is_int($data['discount'])) {
                return $this->json([
                    'message' => 'Discount should be provided'
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $voucher = new Voucher($data['discount']);

            $this->voucherRepository->save(entity: $voucher, flush: true);

            return $this->json([
                'code' => $voucher->getCode()
            ], Response::HTTP_OK);

        } catch (JsonException $e) {
            return $this->json([
                'message' => $e->getMessage()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
