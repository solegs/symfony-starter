<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    public function __construct(private readonly UserService $userService)
    {
    }

    #[Route('/is-new-user', name: 'is_new_user', methods: ['POST'])]
    public function isNewUser(Request $request): Response
    {
        $requestData = json_decode($request->getContent(), true, flags: JSON_THROW_ON_ERROR);

        $tokenOrIpAddress = $requestData['token'] ?? $request->getClientIp();

        if (!$tokenOrIpAddress) {
            return $this->json(['error' => 'Invalid request'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['isNewUser' => $this->userService->isNewUser($tokenOrIpAddress)]);
    }
}
