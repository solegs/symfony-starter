<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Repository\VoucherRepository;
use App\Service\DiscountService;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApplyVoucherController extends AbstractController
{
    public function __construct(
        private readonly VoucherRepository $voucherRepository,
        private readonly DiscountService $discountService
    ) {
    }

    #[Route('/apply', name: 'apply_discount', methods: ['POST'])]
    public function index(Request $request): Response
    {
        $json = $request->getContent();

        try {
            $data = json_decode($json, true, flags: JSON_THROW_ON_ERROR);

            if(empty($data['items']) || !is_iterable($data['items'])) {
                return $this->json([
                    'message' => 'Array of items should be provided'
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if(empty($data['code']) || !is_string($data['code'])) {
                return $this->json([
                    'message' => 'Voucher code should be provided'
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $voucher = $this->voucherRepository->findOneByCode($data['code']);

            if(null === $voucher) {
                return $this->json([
                    'message' => "Voucher with code {$data['code']} not found"
                ], Response::HTTP_NOT_FOUND);
            }

            return $this->json([
                'items' => $this->discountService->applyDiscount($data['items'], $voucher->getDiscount()),
                'code' => $voucher->getCode()
            ], Response::HTTP_OK);

        } catch (JsonException $e) {
            return $this->json([
                'message' => $e->getMessage()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
