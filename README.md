### Alias
```ini
alias dc="docker-compose"
alias o-composer="docker-compose exec php-fpm composer"
alias o-php="docker-compose exec php-fpm php"
alias o-phpunit="docker-compose exec php-fpm vendor/bin/phpunit"
```

## First deploy
```console
cp .env .env.local
dc pull
dc up -d --build
o-composer install
```

## ACCESS
`http://localhost:8023/`

### Testing
```
o-phpunit --stop-on-failure
```